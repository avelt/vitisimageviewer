#' The application server-side
#' 
#' @param input,output,session Internal parameters for {shiny}. 
#'     DO NOT REMOVE.
#' @import shiny
#' @import readxl
#' @import tidyverse
#' @noRd
app_server <- function( input, output, session ) {
  readme=read.csv(paste(system.file("extdata/",package = "VitisImageViewer"),"/readme.csv",sep=""), sep=";", header=T)
  metadata_images=readxl::read_xlsx((paste(system.file("extdata/",package = "VitisImageViewer"),"/Liste_photos.xlsx",sep="")), progress = readxl_progress())
  metadata_images=metadata_images %>% 
    mutate_at(vars(Periode,Vue,Code_plante_mere,QTL_mildiou,QTL_oidium,OIV452_6DPI,X1_OIV452_6DPI,AUDPC_OIV452_6DPI,AUDPC_X1_OIV452_6DPI), factor)
  mod_DTable_server("ImageViewer", image_df = metadata_images, readmefile = readme)
}
